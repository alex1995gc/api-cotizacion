<?php 
	$data = json_decode(file_get_contents("https://dolar.melizeche.com/api/1.0/"),true);
	echo "<h1>Cotización del Dólar en Guaraníes</h1>";
	foreach($data["dolarpy"] as $lugar=>$dolar){
		if($dolar["compra"] == 0 && $dolar["venta"] == 0){
			echo strtoupper($lugar) . " no tiene datos actualmente. <br><br>";
			continue;
		}
		echo  "En " .strtoupper($lugar) . " el dólar está " . $dolar["compra"] . " Gs la Compra, ". $dolar["venta"] . " Gs la Venta.";
		echo "<br><br>";
	}
	echo "Última actualización " . $data["updated"];
 ?>